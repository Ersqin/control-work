﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransactionsController : Controller
    {

        private ApplicationContext context;
        private UserManager<User> userManager;

        public TransactionsController(ApplicationContext context,
            UserManager<User> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }
        [HttpGet]
        public IActionResult Transaction()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Transaction(TransactionViewModel.TransactionResultViewModel model)
        {

            User sender = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            User reciver = context.Users.FirstOrDefault(u => u.UserName == model.ReciverAccount);
            Transactions transaction = new Transactions()
            {
                SendDateTime= DateTime.Now,
                UserReceiverId = reciver.Id,
                SendSum = model.Sum,
                UserSenderId = sender.Id
            };
            sender.PersonalBalance = sender.PersonalBalance - transaction.SendSum;
            reciver.PersonalBalance= reciver.PersonalBalance + transaction.SendSum;
            context.SaveChanges();

            return RedirectToAction("Transaction", "Transactions");
        }

        public IActionResult TransactionLog(LogTransactionViewModel model)
        {
            var transactionses = context.Transactionses.Include(s => s.UserSender)
                .Include(r => r.UserReceiver).Where(d => d.SendDateTime >= model.withDate && d.SendDateTime < model.beforeDate).ToList();
            return View(transactionses);
        }


    }
}
