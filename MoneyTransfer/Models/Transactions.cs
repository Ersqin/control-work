﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transactions
    {
        public int Id { get; set; }

        public string UserSenderId { get; set; }
        public User UserSender { get; set; }
        public string UserReceiverId { get; set; }
        public User UserReceiver { get; set; }
        public int SendSum { get; set; }

        public DateTime SendDateTime { get; set; }

    }
}
